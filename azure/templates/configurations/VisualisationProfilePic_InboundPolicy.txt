<!--
    IMPORTANT:
    - Policy elements can appear only within the <inbound>, <outbound>, <backend> section elements.
    - To apply a policy to the incoming request (before it is forwarded to the backend service), place a corresponding policy element within the <inbound> section element.
    - To apply a policy to the outgoing response (before it is sent back to the caller), place a corresponding policy element within the <outbound> section element.
    - To add a policy, place the cursor at the desired insertion point and select a policy from the sidebar.
    - To remove a policy, delete the corresponding policy statement from the policy document.
    - Position the <base> element within a section element to inherit all policies from the corresponding section element in the enclosing scope.
    - Remove the <base> element to prevent inheriting policies from the corresponding section element in the enclosing scope.
    - Policies are applied in the order of their appearance, from the top down.
    - Comments within policy elements are not supported and may disappear. Place your comments between policy elements or at a higher level scope.
-->
<policies>
<inbound>
        <base />
        <send-request ignore-error="true" timeout="20" response-variable-name="bearerToken" mode="new">
            <set-url>{{sfoauthurl}}</set-url>
            <set-method>POST</set-method>
            <set-header name="Content-Type" exists-action="override">
                <value>application/x-www-form-urlencoded</value>
            </set-header>
            <set-body>@{
		
           return string.Format("client_id={0}&client_secret={1}&username={2}&password={3}&grant_type=password", "{{sfclientid}}", "{{sfclientsecret}}", "{{sfintegrationuser}}", "{{sfintegrationpassword}}");
            }</set-body>
        </send-request>
        <set-variable name="sfoauth" value="@((String)((IResponse)context.Variables["bearerToken"]).Body.As<JObject>()["access_token"])" />
        <choose>
            <when condition="@((((String)context.Variables["sfoauth"])!=null)&&((String)context.Variables["sfoauth"]).Length>0)">
                <set-backend-service base-url="https://mirion--IOTDev--c.cs27.content.force.com/profilephoto" />
                <set-header name="Authorization" exists-action="override">
                    <value>@("Bearer " +((String)context.Variables["sfoauth"]) )</value>
                </set-header>
            </when>
            <otherwise>
                <return-response>
                    <set-status code="401" reason="Credential Invalid" />
                    <set-body>UnAuthorised</set-body>
                </return-response>
            </otherwise>
        </choose>
    </inbound>
    </policies>