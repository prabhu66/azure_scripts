[
  {
    "name": "DB_HOST",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "DB_NAME",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "DB_PASS",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "DB_USER",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "DEADLEAD_QUEUE_NAME",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "ENABLE_APP_LOG",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "ikey",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "SALESFORCE_API_AUTH_URI",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "SALESFORCE_API_CASE_URI",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "SALESFORCE_API_CLIENTID",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "SALESFORCE_API_HIERARCHY_URI",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "SALESFORCE_API_PASSWORD",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "SALESFORCE_API_SECRETID",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "SALESFORCE_API_USERNAME",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "STORAGE_ACCOUNT_KEY",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "STORAGE_ACCOUNT_NAME",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "TABLE_DEVICE_CONFIG",
    "value": "",
    "slotSetting": false
  },
  {
    "name": "UNAUTH_QUEUE_NAME",
    "value": "",
    "slotSetting": false
  }
]