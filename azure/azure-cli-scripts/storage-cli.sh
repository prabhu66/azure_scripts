AZURE_APP_ID=${AZURE_APP_ID:?'AZURE_APP_ID variable missing.'}
AZURE_PASSWORD=${AZURE_PASSWORD:?'AZURE_PASSWORD variable missing.'}
AZURE_TENANT_ID=${AZURE_TENANT_ID:?'AZURE_TENANT_ID variable missing.'}
AZURE_COMMONS_RG=${AZURE_COMMONS_RG:?'AZURE_COMMONS_RG variable missing.'}
AZURE_SFI_RG=${AZURE_SFI_RG:?'AZURE_SFI_RG variable missing.'}
AZURE_STORAGE_RG=${AZURE_STORAGE_RG:?'AZURE_STORAGE_RG variable missing.'}
AZURE_TELEMETRY_RG=${AZURE_TELEMETRY_RG:?'AZURE_TELEMETRY_RG variable missing.'}
AZURE_VISUALISATION_RG=${AZURE_VISUALISATION_RG:?'AZURE_VISUALISATION_RG variable missing.'}
AZURE_RESOURCE_LOCATION=${AZURE_RESOURCE_LOCATION:?'AZURE_RESOURCE_LOCATION variable missing.'}
AZURE_STORAGE_ACC=${AZURE_STORAGE_ACC:?'AZURE_STORAGE_ACC variable missing.'}
AZURE_MYSQL_SERVER=${AZURE_MYSQL_SERVER:?'AZURE_MYSQL_SERVER variable missing.'}
MYSQL_UNAME=${MYSQL_UNAME:?'MYSQL_UNAME variable missing.'}
MYSQL_PASSWORD=${MYSQL_PASSWORD:?'MYSQL_PASSWORD variable missing.'}

AUTH_ARGS_STRING="--username ${AZURE_APP_ID} --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}"

#Azure Login
az login --service-principal ${AUTH_ARGS_STRING}

#Storage Account Key
STORAGEKEY=$(az storage account keys list --resource-group $AZURE_STORAGE_RG --account-name $AZURE_STORAGE_ACC --query "[0].value" | tr -d '"')

#Storage Account table creation
az storage table create --name DeviceConfiguration --account-name ${AZURE_STORAGE_ACC} --account-key $STORAGEKEY

az storage table create --name DeviceLatestTelemetry --account-name ${AZURE_STORAGE_ACC} --account-key $STORAGEKEY

az storage table create --name Devicemetrics --account-name ${AZURE_STORAGE_ACC} --account-key $STORAGEKEY

az storage table create --name SendEmail --account-name ${AZURE_STORAGE_ACC} --account-key $STORAGEKEY

az storage table create --name DeviceLatestCustomAlarms --account-name ${AZURE_STORAGE_ACC} --account-key $STORAGEKEY

az storage table create --name ParametersThresholdAudit --account-name ${AZURE_STORAGE_ACC} --account-key $STORAGEKEY

az storage table create --name CertificateStatusTrack --account-name ${AZURE_STORAGE_ACC} --account-key $STORAGEKEY

az storage queue create --name devlpqaccountdlq --account-name ${AZURE_STORAGE_ACC} --account-key $STORAGEKEY
#MySQL Server creation
az mysql server create --resource-group ${AZURE_STORAGE_RG} --name ${AZURE_MYSQL_SERVER} --location ${AZURE_RESOURCE_LOCATION} --admin-user ${MYSQL_UNAME} --admin-password ${MYSQL_PASSWORD} --sku-name B_Gen5_1 --version 5.7 --ssl-enforcement Disabled