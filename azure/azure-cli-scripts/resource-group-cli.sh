AZURE_APP_ID=${AZURE_APP_ID:?'AZURE_APP_ID variable missing.'}
AZURE_PASSWORD=${AZURE_PASSWORD:?'AZURE_PASSWORD variable missing.'}
AZURE_TENANT_ID=${AZURE_TENANT_ID:?'AZURE_TENANT_ID variable missing.'}
AZURE_COMMONS_RG=${AZURE_COMMONS_RG:?'AZURE_COMMONS_RG variable missing.'}
AZURE_SFI_RG=${AZURE_SFI_RG:?'AZURE_SFI_RG variable missing.'}
AZURE_STORAGE_RG=${AZURE_STORAGE_RG:?'AZURE_STORAGE_RG variable missing.'}
AZURE_TELEMETRY_RG=${AZURE_TELEMETRY_RG:?'AZURE_TELEMETRY_RG variable missing.'}
AZURE_VISUALISATION_RG=${AZURE_VISUALISATION_RG:?'AZURE_VISUALISATION_RG variable missing.'}
AZURE_RESOURCE_LOCATION=${AZURE_RESOURCE_LOCATION:?'AZURE_RESOURCE_LOCATION variable missing.'}


AUTH_ARGS_STRING="--username ${AZURE_APP_ID} --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}"

az login --service-principal ${AUTH_ARGS_STRING}

AZURE_RESOURCE_GROUP_EXISTS=$(az group exists --name ${AZURE_COMMONS_RG})

if [[ $AZURE_RESOURCE_GROUP_EXISTS == "false" ]]; then

	az group create --name ${AZURE_COMMONS_RG} --location ${AZURE_RESOURCE_LOCATION}

fi

AZURE_RESOURCE_GROUP_EXISTS=$(az group exists --name ${AZURE_SFI_RG})

if [[ $AZURE_RESOURCE_GROUP_EXISTS == "false" ]]; then

	az group create --name ${AZURE_SFI_RG} --location ${AZURE_RESOURCE_LOCATION}
	AZURE_RESOURCE_GROUP_EXISTS='false'

fi

AZURE_RESOURCE_GROUP_EXISTS=$(az group exists --name ${AZURE_STORAGE_RG})

if [[ $AZURE_RESOURCE_GROUP_EXISTS == "false" ]]; then

	az group create --name ${AZURE_STORAGE_RG} --location ${AZURE_RESOURCE_LOCATION}
	AZURE_RESOURCE_GROUP_EXISTS='false'

fi

AZURE_RESOURCE_GROUP_EXISTS=$(az group exists --name ${AZURE_TELEMETRY_RG})

if [[ $AZURE_RESOURCE_GROUP_EXISTS == "false" ]]; then

	az group create --name ${AZURE_TELEMETRY_RG} --location ${AZURE_RESOURCE_LOCATION}
	AZURE_RESOURCE_GROUP_EXISTS='false'

fi

AZURE_RESOURCE_GROUP_EXISTS=$(az group exists --name ${AZURE_VISUALISATION_RG})

if [[ $AZURE_RESOURCE_GROUP_EXISTS == "false" ]]; then

	az group create --name ${AZURE_VISUALISATION_RG} --location ${AZURE_RESOURCE_LOCATION}

fi 

if [[ $AZURE_RESOURCE_GROUP_EXISTS == "false" ]]; then

	az group create --name ${AZURE_CERTIFICATE_RG} --location ${AZURE_RESOURCE_LOCATION}
	AZURE_RESOURCE_GROUP_EXISTS='false'

fi
AZURE_RESOURCE_GROUP_EXISTS=$(az group exists --name ${AZURE_CERTIFICATE_RG})