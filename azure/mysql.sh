#!/bin/bash

AZURE_APP_ID=${AZURE_APP_ID:?'AZURE_APP_ID variable missing.'}
AZURE_PASSWORD=${AZURE_PASSWORD:?'AZURE_PASSWORD variable missing.'}
AZURE_TENANT_ID=${AZURE_TENANT_ID:?'AZURE_TENANT_ID variable missing.'}
AZURE_STORAGE_RG=${AZURE_STORAGE_RG:?'AZURE_STORAGE_RG variable missing.'}
AZURE_MYSQL_SERVER=${AZURE_MYSQL_SERVER:?'AZURE_MYSQL_SERVER variable missing.'} 
MYSQL_UNAME=${MYSQL_UNAME:?'MYSQL_UNAME variable missing.'}
MYSQL_PASSWORD=${MYSQL_PASSWORD:?'MYSQL_PASSWORD variable missing.'}
#AZURE_MYSQL=uat-lp-db-main
#AZURE_STORAGE_RG=Mirion

#ifconfig 

#IP=$(hostname -i)

#echo $IP

AUTH_ARGS_STRING="--username ${AZURE_APP_ID} --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}"

#Azure Login
az login --service-principal ${AUTH_ARGS_STRING}

az mysql server firewall-rule create --resource-group ${AZURE_STORAGE_RG} --server ${AZURE_MYSQL_SERVER} --name BitbucketPipe --start-ip-address 0.0.0.0 --end-ip-address 255.255.255.255

#Commands to execute mysql scripts
apk update

apk add mysql-client

cp mysql_scripts/*.sql .

mysql -h ${AZURE_MYSQL_SERVER}.mysql.database.azure.com -u ${MYSQL_UNAME}@${AZURE_MYSQL_SERVER} -P 3306 -p${MYSQL_PASSWORD} < main.sql

az mysql server firewall-rule delete --resource-group ${AZURE_STORAGE_RG} --server-name ${AZURE_MYSQL_SERVER} --name BitbucketPipe --yes