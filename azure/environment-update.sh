#!/bin/bash

AZURE_APP_ID=${AZURE_APP_ID:?'AZURE_APP_ID variable missing.'}
AZURE_PASSWORD=${AZURE_PASSWORD:?'AZURE_PASSWORD variable missing.'}
AZURE_TENANT_ID=${AZURE_TENANT_ID:?'AZURE_TENANT_ID variable missing.'}
DEPLOY_ENVIRONMENT=${DEPLOY_ENVIRONMENT:?'DEPLOY_ENVIRONMENT variable missing.'}
SUBSCRIPTION_ID=${SUBSCRIPTION_ID:?'SUBSCRIPTION_ID variable missing.'}
OBJECT_ID=${OBJECT_ID:?'OBJECT_ID variable missing.'}
AZURE_RESOURCE_LOCATION=${AZURE_RESOURCE_LOCATION:?'AZURE_RESOURCE_LOCATION variable missing.'}

cp templates/lp-* .


#logging Environment
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-logginganalytics-parameter.json
sed -i "s/<AZURE_TENANT_ID>/$AZURE_TENANT_ID/g" lp-logginganalytics-template.json
sed -i "s/<AZURE_RESOURCE_LOCATION>/$AZURE_RESOURCE_LOCATION/g" lp-logginganalytics-template.json


#Common Environment
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-common-parameter.json
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-common-template.json
sed -i "s/<AZURE_TENANT_ID>/$AZURE_TENANT_ID/g" lp-common-template.json
sed -i "s/<OBJECT_ID>/$OBJECT_ID/g" lp-common-template.json
sed -i "s/<AZURE_RESOURCE_LOCATION>/$AZURE_RESOURCE_LOCATION/g" lp-common-template.json

#APIM Environment
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-apim-parameters.json
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-apim-template.json
sed -i "s/<AZURE_TENANT_ID>/$AZURE_TENANT_ID/g" lp-apim-template.json
sed -i "s/<OBJECT_ID>/$OBJECT_ID/g" lp-apim-template.json
sed -i "s/<AZURE_RESOURCE_LOCATION>/$AZURE_RESOURCE_LOCATION/g" lp-apim-template.json

#APIM Properties
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-apimproperties-parameters.json
sed -i "s/<azureportaljwtsecretbase64>/$AZUREPORTALJWTSECRETBASE64/g" lp-apimproperties-template.json
sed -i "s|<labpulseportalurlforcors>|$LABPULSEPORTALURLFORCORS|g" lp-apimproperties-template.json
sed -i "s/<sfclientid>/$SALESFORCE_AUTH_CLIENTID/g" lp-apimproperties-template.json
sed -i "s/<sfclientsecret>/$SALESFORCE_AUTH_SECRETID/g" lp-apimproperties-template.json
sed -i "s|<sfoauthurl>|$SALESFORCE_AUTH_URL|g" lp-apimproperties-template.json
sed -i "s|<sfprofilepicbaseurl>|$SFPROFILEPICBASEURL|g" lp-apimproperties-template.json
sed -i "s|<salesforceimagesbaseurl>|$SALESFORCE_IMAGES_BASEURL|g" lp-apimproperties-template.json
sed -i "s/<sfintegrationpassword>/$SFINTEGRATIONPASSWORD/g" lp-apimproperties-template.json
sed -i "s/<sfintegrationuser>/$SFINTEGRATIONUSER/g" lp-apimproperties-template.json




#Storage Environment
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-storage-parameter.json
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-storage-template.json
sed -i "s/<MYSQL_UNAME>/$MYSQL_UNAME/g" lp-storage-template.json
sed -i "s/<MYSQL_PASSWORD>/$MYSQL_PASSWORD/g" lp-storage-template.json
sed -i "s/<MYSQL_DBNAME>/$MYSQL_DBNAME/g" lp-storage-template.json
sed -i "s/<AZURE_MYSQL_SERVER>/$AZURE_MYSQL_SERVER/g" lp-storage-template.json
sed -i "s/<AZURE_RESOURCE_LOCATION>/$AZURE_RESOURCE_LOCATION/g" lp-storage-template.json


#Telemetry Environment
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-telemetryingestion-parameter.json
sed -i "s/<SUBSCRIPTION_ID>/$SUBSCRIPTION_ID/g" lp-telemetryingestion-parameter.json
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-telemetryingestion-template.json
sed -i "s/<AZURE_RESOURCE_LOCATION>/$AZURE_RESOURCE_LOCATION/g" lp-telemetryingestion-template.json

#Stream Analytics Environment
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-streamanalytics-parameter.json
sed -i "s/<SUBSCRIPTION_ID>/$SUBSCRIPTION_ID/g" lp-streamanalytics-parameter.json
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-streamanalytics-template.json
sed -i "s/<AZURE_RESOURCE_LOCATION>/$AZURE_RESOURCE_LOCATION/g" lp-streamanalytics-template.json


#SF integration Environment
sed -i "s/<SUBSCRIPTION_ID>/$SUBSCRIPTION_ID/g" lp-sfintegration-parameter.json
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-sfintegration-parameter.json
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-sfintegration-template.json
sed -i "s/<AZURE_RESOURCE_LOCATION>/$AZURE_RESOURCE_LOCATION/g" lp-sfintegration-template.json

#Visualisation Environment
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-visualisation-parameter.json
sed -i "s/<SUBSCRIPTION_ID>/$SUBSCRIPTION_ID/g" lp-visualisation-parameter.json
sed -i "s/<apimipaddress>/$APIMIPADDRESS/g" lp-visualisation-parameter.json
sed -i "s/<APIM_SUBSCRIPTION_KEY>/$APIM_SUBSCRIPTION_KEY/g" lp-visualisation-template.json
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-visualisation-template.json
sed -i "s/<AZURE_RESOURCE_LOCATION>/$AZURE_RESOURCE_LOCATION/g" lp-visualisation-template.json

#KeyVault Ingestion
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-keyvault-parameter.json
sed -i "s/<DEPLOY_ENVIRONMENT>/$DEPLOY_ENVIRONMENT/g" lp-keyvault-template.json
sed -i "s/<APIM_SUBSCRIPTION_KEY>/$APIM_SUBSCRIPTION_KEY/g" lp-keyvault-template.json
sed -i "s/<SALESFORCE_AUTH_CLIENTID>/$SALESFORCE_AUTH_CLIENTID/g" lp-keyvault-template.json
sed -i "s/<SALESFORCE_AUTH_SECRETID>/$SALESFORCE_AUTH_SECRETID/g" lp-keyvault-template.json
sed -i "s/<SALESFORCE_AUTH_TOKEN_SECRET>/$SALESFORCE_AUTH_TOKEN_SECRET/g" lp-keyvault-template.json
sed -i "s|<SALESFORCE_AUTH_URL>|$SALESFORCE_AUTH_URL|g" lp-keyvault-template.json
sed -i "s|<SALESFORCE_CASE_URL>|$SALESFORCE_CASE_URL|g" lp-keyvault-template.json
sed -i "s|<SALESFORCE_COMMUNITYUSER_URL>|$SALESFORCE_COMMUNITYUSER_URL|g" lp-keyvault-template.json
sed -i "s|<SALESFORCE_FORGOT_PWD_COMMUNITY_URL>|$SALESFORCE_FORGOT_PWD_COMMUNITY_URL|g" lp-keyvault-template.json
sed -i "s|<SALESFORCE_FORGOT_PWD_STDUSER_URL>|$SALESFORCE_FORGOT_PWD_STDUSER_URL|g" lp-keyvault-template.json
sed -i "s|<SALESFORCE_HIERARCHY_URL>|$SALESFORCE_HIERARCHY_URL|g" lp-keyvault-template.json
sed -i "s/<SALESFORCE_PASSWORD>/$SALESFORCE_PASSWORD/g" lp-keyvault-template.json
sed -i "s|<SALESFORCE_QUERY_URL>|$SALESFORCE_QUERY_URL|g" lp-keyvault-template.json
sed -i "s/<SALESFORCE_REFRESH_TOKEN_SECRET>/$SALESFORCE_REFRESH_TOKEN_SECRET/g" lp-keyvault-template.json
sed -i "s|<SALESFORCE_USER_PERMISSION_URL>|$SALESFORCE_USER_PERMISSION_URL|g" lp-keyvault-template.json
sed -i "s/<SALESFORCE_USERNAME>/$SALESFORCE_USERNAME/g" lp-keyvault-template.json
sed -i "s/<SENDGRID_SECRET_KEY>/$SENDGRID_SECRET_KEY/g" lp-keyvault-template.json
