

USE `labpulse` ;

-- -----------------------------------------------------
-- procedure sp_get_applicationversion
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$

CREATE  PROCEDURE `sp_get_applicationversion`()
BEGIN

SELECT ApplicationName,ApplicationLabel,version FROM `labpulse`.`applicationversion`;
 

END$$
DELIMITER ;


-- -----------------------------------------------------
-- procedure sp_syncapplicationversion
-- -----------------------------------------------------

DELIMITER $$
CREATE PROCEDURE `sp_syncapplicationversion`(
  in_ApplicationName varchar(200),
in_ApplicationLabel varchar(200),
in_Version varchar(200),
  out record_count varchar(11),
  out row_id varchar(200),
  out statuscode integer(10),
  out message varchar(100))
BEGIN
DECLARE code CHAR(5) DEFAULT '0';
DECLARE msg TEXT;
DECLARE rows INT;
DECLARE paramsexist int;

/*DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'Error occured';*/
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
    END;
		SELECT count(*) into paramsexist FROM    applicationversion where ApplicationName= in_ApplicationName;
			if(paramsexist<1) then
				Begin
				INSERT INTO `applicationversion`(`ApplicationName`,`ApplicationLabel`,`version`) VALUES(in_ApplicationName,in_ApplicationLabel,in_version);
				End;
			else
				Begin
				UPDATE `applicationversion` SET  `ApplicationLabel` = in_ApplicationLabel,`version` = in_version WHERE `ApplicationName` =in_ApplicationName;
				End;
			END IF;
IF code = '0' THEN
    GET DIAGNOSTICS rows = ROW_COUNT;
    SET record_count = rows; 
	select LAST_INSERT_ID() into row_id from labpulse.applicationversion;
  ELSE
    set statuscode = code;
    set message = msg;
  END IF;
END$$
DELIMITER ;


-- -----------------------------------------------------
-- procedure sp_get_DevicesByCodeandLocation
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_get_DevicesByCodeandLocation`(in_locationid varchar(45),in_productCode varchar(1000), in_accountid varchar(45),
   out statuscode integer(10),
  out message varchar(100))
BEGIN

if(in_accountid!='null') then
 Begin
	SELECT * FROM labpulse.installed_product where accountID=in_accountid and locationID=in_locationid and FIND_IN_SET(productCode,in_productCode);
 End;
else
	Begin
		SELECT * FROM labpulse.installed_product where locationID=in_locationid and FIND_IN_SET(productCode,in_productCode);
	End;
End if;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_deviceDetailedDescription
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_get_deviceDetailedDescription`(in_serialnumber varchar(45),in_productcode varchar(45),
in_AccountID varchar(45),
   out statuscode integer(10),
  out message varchar(100))
BEGIN
Declare isValidAccount INT;
SELECT count(1) into isValidAccount FROM labpulse.installed_product where serialNumber=in_serialnumber and productCode=in_productcode and accountID=in_AccountID;
	if(isValidAccount>0 OR length(in_AccountID)=0 OR in_AccountID='null') then
		Begin
			set statuscode=0;
			select * from labpulse.deviceparamsthreshold where serialNumber=in_serialnumber and productCode=in_productcode;
		END;
	else
		begin
		set statuscode=-1;
		set message='Invalid Account';
		End;
	End if;
End$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_device_details
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_get_device_details`(
  in in_Serialnumber varchar(50),
  in in_ProductCode varchar(50),
   out statuscode integer(10),
  out message varchar(100))
BEGIN

SELECT 
    `installed_product`.`serialNumber`,
    `installed_product`.`installedProductID`,
    `installed_product`.`productCode`,
    `installed_product`.`productName`,
    `installed_product`.`baseProductId`,
    `installed_product`.`baseProductName`,
    `installed_product`.`accountID`,
    `installed_product`.`locationID`,
    `installed_product`.`preferredTechnicain`,
    `installed_product`.`contactName`,
    `installed_product`.`contactEmail`,
    `installed_product`.`contactID`,
    `installed_product`.`contactMobileNo`,
    `installed_product`.`topLevelSerialNumber`,
    `installed_product`.`parentSerialNumber`,
    location.LocationName
   
FROM `installed_product` 
left outer join location on `installed_product`.locationID=location.LocationID
 where `installed_product`.productCode=in_ProductCode  and `installed_product`.serialNumber=in_Serialnumber ;
 

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_device_master_details
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_get_device_master_details`(in_deviceId varchar(45),
  in_productCode varchar(45),
  out statuscode integer(10),
  out message varchar(100))
BEGIN
SELECT 'devices' as devicetype,imageURL,serialNumber,installedProductID,productCode,productName from installed_product where  serialNumber=in_deviceId and productCode=in_productCode;
 
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_device_params_threshold
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_get_device_params_threshold`(
   out statuscode integer(10),
  out message varchar(100))
BEGIN

SELECT
	
    `deviceparamsthreshold`.`SerialNumber`,
    `deviceparamsthreshold`.`AlarmHighLevelThreshold`,
    `deviceparamsthreshold`.`AlarmLowLevelThreshold`,
    `deviceparamsthreshold`.`AlertHighLevelThreshold`,
    `deviceparamsthreshold`.`AlertLowLevelThreshold`,
    `deviceparamsthreshold`.`ProductCode`,
    `deviceparamsthreshold`.`DeviceName`,
    `deviceparamsthreshold`.`IsCustomAlarmEnable`,
    `deviceparamsthreshold`.`ParamName`,
    `deviceparamsthreshold`.`CreatedOn`,
    `deviceparamsthreshold`.`LastUpdatedOn`
    
FROM `deviceparamsthreshold` ;
 /*where `deviceparamsthreshold`.IsCustomAlarmEnable in("true",1) ;*/
 

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_device_threshold_lastupdated_time
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_get_device_threshold_lastupdated_time`(
   out statuscode integer(10),
  out message varchar(100))
BEGIN

SELECT
	
    `deviceparamsthreshold`.`SerialNumber`,
    `deviceparamsthreshold`.`AlarmHighLevelThreshold`,
    `deviceparamsthreshold`.`AlarmLowLevelThreshold`,
    `deviceparamsthreshold`.`AlertHighLevelThreshold`,
    `deviceparamsthreshold`.`AlertLowLevelThreshold`,
    `deviceparamsthreshold`.`productCode`,
    `deviceparamsthreshold`.`DeviceName`,
    `deviceparamsthreshold`.`IsCustomAlarmEnable`,
    `deviceparamsthreshold`.`ParamName`,
    `deviceparamsthreshold`.`CreatedOn`,
    `deviceparamsthreshold`.`LastUpdatedOn`
    
FROM `deviceparamsthreshold` ORDER BY `deviceparamsthreshold`.`LastUpdatedOn` DESC LIMIT 1 ;
 

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_gateway_details
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_get_gateway_details`(
  in in_Serialnumber varchar(50),
   out statuscode integer(10),
  out message varchar(100))
BEGIN

SELECT 
    `installed_product`.`serialNumber`,
    `installed_product`.`installedProductID`,
    `installed_product`.`productCode`,
    `installed_product`.`productName`,
    `installed_product`.`baseProductId`,
    `installed_product`.`baseProductName`,
    `installed_product`.`accountID`,
    `installed_product`.`locationID`,
    `installed_product`.`preferredTechnicain`,
    `installed_product`.`contactName`,
    `installed_product`.`contactEmail`,
    `installed_product`.`contactID`,
    `installed_product`.`contactMobileNo`,
    `installed_product`.`topLevelSerialNumber`,
    `installed_product`.`parentSerialNumber`
   
FROM `installed_product` where serialNumber=in_Serialnumber and 
 GatewayType is not null and length(GatewayType)>0 ;
 

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_gateway_master
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_get_gateway_master`(
  in in_gatewayId varchar(50),
   out statuscode integer(10),
  out message varchar(100))
BEGIN

SELECT     serialNumber, installedProductID,productCode, prodDescription as productName  FROM installed_product 
where locationID in (select locationID from installed_product where  serialNumber=in_gatewayId) and 
 serialNumber is not null and length(serialNumber)>1;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_hirarchySearch
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_get_hirarchySearch`(in_id varchar(45),in_text varchar(45),
   out statuscode integer(10),
  out message varchar(100))
BEGIN
	if in_text = 'Account' then               
				              
	 select * from labpulse.account;
			
	Elseif in_text = 'Location' then
			
	 select * from labpulse.location where AccountID=in_id;
				
	Elseif in_text = 'Device' then
			
	  select DISTINCT productCode,locationID from labpulse.installed_product where locationID=in_id and serialNumber is NOT NULL;
	  
	  	Elseif in_text = 'Devices' then
			
	  select * from labpulse.installed_product where productCode=in_id and serialNumber is NOT NULL;
	            
	end if;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_installedproduct_forReferenceBlob
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_get_installedproduct_forReferenceBlob`(
  out serialNumber varchar(11),
  out installedProductID varchar(11),
  out productCode integer(10),
  out productName integer(10),
  out baseProductId varchar(100),
  out baseProductName integer(10),
  out accountID integer(10),
  out locationID integer(10),
  out preferredTechnician varchar(100),
  out contactName varchar(100),
  out contactEmail integer(150),
  out contactMobileNo integer(10),
  out topLevelSerialNumber integer(10),
  out parentSerialNumber varchar(10),  
  out ImageURL varchar(500),
  out isLabPulseReady integer(10),
  out isTopLevel integer(10),
  out isOffline varchar(10),
  out labPulseConnected varchar(20),
  out statuscode integer(10),
  out message varchar(100))
BEGIN
declare lastfetchtime datetime;
declare updatedrecordcount int default 0;
select lastMySqlFetchedTime into lastfetchtime from referenceblobconfig;
select count(*) into updatedrecordcount from labpulse.installed_product where modifiedDate>lastfetchtime;
if(updatedrecordcount>0) then
 SELECT * from labpulse.installed_product;
end if;
 
/*SELECT * from labpulse.installed_product;*/
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_locationdetail
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_get_locationdetail`(in_AccountID varchar(45),
   out statuscode integer(10),
  out message varchar(100))
BEGIN

set statuscode =999;
 SELECT LocationID, LocationName, Latitude,Longitude, City, Country, ImageURL from labpulse.location where AccountID=in_AccountID;
 
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_systemandgatewayinlocation
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_get_systemandgatewayinlocation`(in_locationid varchar(45),
  out statuscode integer(10),
  out message varchar(100))
BEGIN
 select 'gateway' as entitytype ,serialNumber,installedProductID ,productCode,prodDescription as  entityName,ImageURL ,null as sensorsystem from installed_product where locationID=in_locationid and productCode in ('IGT-20' ,'LP-GATEWAY-ETH' )
union 
select 'system' as entitytype ,serialNumber ,installedProductID,productCode,prodDescription as  entityName,ImageURL  ,sensorSystemID as sensorsystem from installed_product where locationID=in_locationid and isTopLevel='true'
union
select 'location' as entitytype ,null as serialNumber ,null as installedProductID,null as productCode,LocationName as entityName,ImageURL, null as sensorsystem from location where LocationID=in_locationid;
 
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_get_systemdetail
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_get_systemdetail`(in_systemId varchar(45),
  out statuscode integer(10),
  out message varchar(100))
BEGIN


SELECT 'devices' as devicetype,imageURL,serialNumber,installedProductID,productCode,prodDescription as productName,null as sensorSystemID from installed_product where ( topLevelSerialNumber=in_systemId 
or topLevelSerialNumber in(select sensorSystemID from installed_product where  installedProductID=in_systemId) )and length(serialNumber)>0  and topLevelSerialNumber !='null'
UNION
SELECT 'system' as devicetype,imageURL,serialNumber,installedProductID,productCode,prodDescription as productName,sensorSystemID from installed_product where  installedProductID=in_systemId;

 
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_getprodhierarchy
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_getprodhierarchy`(in_accountid varchar(45),
   out statuscode integer(10),
  out message varchar(100))
BEGIN
if(length(in_accountid)>0)
Then
	select CTE.* ,loc.LocationName,acc.AccountName
	from 
	(
	select DISTINCT(productCode),  locationID ,accountID from labpulse.installed_product 
    where AccountID=in_accountid and  Length(serialNumber)>0
	) CTE inner join location loc  on cte.locationID=loc.LocationID inner join account acc
	 on cte.cte.accountID=acc.AccountID;

else

	select CTE.* ,loc.LocationName,acc.AccountName
	from 
	(
	select DISTINCT(productCode),  locationID ,accountID from labpulse.installed_product 
    where Length(serialNumber)>0
	) CTE inner join location loc  on cte.locationID=loc.LocationID inner join account
	 acc on cte.cte.accountID=acc.AccountID;
end if;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_insert_cases
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_insert_cases`(in_CaseType varchar(45),
  in_SerialNumber varchar(45),
  in_GateWayId varchar(45),
  in_CaseReason varchar(45),
  in_Description varchar(1000),
 in_Subject varchar(200),
  in_AccountID varchar(45),
  in_CaseSFID varchar(45),
  in_LocationName varchar(45),
  in_CaseStatus varchar(45),
  in_CaseCreatedTime datetime,
  in_CaseOrigin varchar(45),
  in_ComponentSN varchar(45),
  in_AlarmType varchar(45),
in_ProductCode varchar(45),
  in_DeviceID varchar(45),
  out record_count varchar(11),
  out row_id integer(10),
  out statuscode integer(10),
  out message varchar(1000))
BEGIN
DECLARE code CHAR(5) DEFAULT '0';
DECLARE msg TEXT;
DECLARE rows INT;
DECLARE result TEXT;
DECLARE existcase INT;

/*DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'Error occured';*/
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
    END;
/* If the combination of Deviceid,ProductCode,AlarmType,CaseStatus!="Closed"
  exists ,we have open case for it so do not insert anything in db and return row_id=0 ,status=sucessfull,record_count=0 
not exists insert into DB and reurn the row_id
 */
SELECT count(*) into existcase FROM    cases where SerialNumber= in_SerialNumber and ProductCode=in_ProductCode and AlarmType=in_AlarmType and CaseStatus!='Closed';
if(existcase<1) then
	Begin
	insert into labpulse.cases
	(DeviceID,ProductCode,CaseType, SerialNumber, GateWayId, CaseReason, Description,Subject, AccountID, CaseSFID, LocationName, CaseStatus, CaseCreatedTime, CaseOrigin, ComponentSN,AlarmType)
	 values
	(in_DeviceID,in_ProductCode,in_CaseType, in_SerialNumber, in_GateWayId, in_CaseReason, in_Description,in_Subject, in_AccountID, in_CaseSFID, in_LocationName, in_CaseStatus, in_CaseCreatedTime, in_CaseOrigin, in_ComponentSN,in_AlarmType);
	 
		 IF code = '0' THEN
			GET DIAGNOSTICS rows = ROW_COUNT;
			SET record_count = rows; 
		/*	select max(ID) into row_id from labpulse.cases;*/
			select last_insert_id() into row_id; 
		  ELSE
			set statuscode = code;
			set message = msg;
			END if;
	End;
Else 
	Begin
	 IF code = '0' THEN
			GET DIAGNOSTICS rows = ROW_COUNT;
			SET record_count = 0; 
			SET row_id = 0;
			
	 ELSE
			set statuscode = code;
			set message = msg;
			END if;
	End;
END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_insert_cases_new
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_insert_cases_new`(in_CaseType varchar(45),
  in_SerialNumber varchar(45),
  in_GateWayId varchar(45),
  in_CaseReason varchar(45),
  in_Description varchar(1000),
 in_Subject varchar(200),
  in_AccountID varchar(45),
  in_CaseSFID varchar(45),
  in_LocationName varchar(45),
  in_CaseStatus varchar(45),
  in_CaseCreatedTime datetime,
  in_CaseOrigin varchar(45),
  in_ComponentSN varchar(45),
  in_AlarmType varchar(45),
in_ProductCode varchar(45),
  in_DeviceID varchar(45),
  out record_count varchar(11),
  out row_id integer(10),
  out statuscode integer(10),
  out message varchar(1000))
BEGIN
DECLARE code CHAR(5) DEFAULT '0';
DECLARE msg TEXT;
DECLARE rows INT;
DECLARE result TEXT;
DECLARE existcase INT;

/*DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'Error occured';*/
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
    END;
/* If the combination of Deviceid,ProductCode,AlarmType,CaseStatus!="Closed"
  exists ,we have open case for it so do not insert anything in db and return row_id=0 ,status=sucessfull,record_count=0 
not exists insert into DB and reurn the row_id
 */
 SET row_id=0;
SELECT count(*) into existcase FROM    cases where SerialNumber= in_SerialNumber and ProductCode=in_ProductCode and AlarmType=in_AlarmType and CaseStatus!='Closed';
if(existcase<1) then
	Begin
	insert into labpulse.cases
	(DeviceID,ProductCode,CaseType, SerialNumber, GateWayId, CaseReason, Description,Subject, AccountID, CaseSFID, LocationName, CaseStatus, CaseCreatedTime, CaseOrigin, ComponentSN,AlarmType)
	 values
	(in_DeviceID,in_ProductCode,in_CaseType, in_SerialNumber, in_GateWayId, in_CaseReason, in_Description,in_Subject, in_AccountID, in_CaseSFID, in_LocationName, in_CaseStatus, in_CaseCreatedTime, in_CaseOrigin, in_ComponentSN,in_AlarmType);
	 
		 IF code = '0' THEN
			GET DIAGNOSTICS rows = ROW_COUNT;
			SET record_count = rows; 
		
			select last_insert_id() into row_id; 
		  ELSE
			set statuscode = code;
			set message = msg;
			END if;
	End;
Else 
	Begin
	SELECT ID into row_id FROM    cases where SerialNumber= in_SerialNumber and ProductCode=in_ProductCode and AlarmType=in_AlarmType and CaseStatus!='Closed' and CaseSFID is null and SFResponse like '%UNABLE_TO_LOCK_ROW%';
	
	 IF code = '0' THEN
			GET DIAGNOSTICS rows = ROW_COUNT;
			SET record_count = 0; 
			
			
	 ELSE
			set statuscode = code;
			set message = msg;
			END if;
	End;
END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_search_DevicesByCodeandSerialNo
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_search_DevicesByCodeandSerialNo`(in_AccountID varchar(45),in_productcode varchar(45),
in_serialnumber varchar(45),
   out statuscode integer(10),
  out message varchar(100))
BEGIN
Declare isValidAccount INT;

SELECT count(1) into isValidAccount FROM labpulse.installed_product where serialNumber=in_serialnumber and productCode=in_productcode and accountID=in_AccountID;
	if(isValidAccount>0 OR length(in_AccountID)=0 OR in_AccountID='null') then
		Begin
			set statuscode=0;
			select * from labpulse.installed_product where serialNumber=in_serialnumber and productCode=in_productcode;
		END;
	else
		begin
			set statuscode=-1;
			set message='Invalid Account';
		End;
	End if;
End$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_syncparametersthreshold
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_syncparametersthreshold`(
  in_SerialNumber varchar(45),
  in_Parameter varchar(45),
  in_ProductCode varchar(45),
  in_IsCustomAlarmEnable varchar(45),
  in_DeviceName varchar(45) ,  
  in_AlertHighLevelThreshold varchar(45) ,
  in_AlertLowLevelThreshold varchar(45) ,
  in_AlarmHighLevelThreshold varchar(45) ,
  in_AlarmLowLevelThreshold varchar(45) ,
  in_AccountID varchar(45),
  out record_count varchar(11),
  out row_id integer(10),
  out statuscode integer(10),
  out message varchar(100))
BEGIN
DECLARE code CHAR(5) DEFAULT '0';
DECLARE msg TEXT;
DECLARE rows INT;
DECLARE paramsexist int;
Declare isValidAccount INT;
/*DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'Error occured';*/
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
    END;

SELECT count(1) into isValidAccount FROM installed_product where serialNumber=in_SerialNumber and productCode=in_ProductCode and accountID=in_AccountID;
	if(isValidAccount>0 OR in_AccountID='null') then
	Begin
		SELECT count(*) into paramsexist FROM    deviceparamsthreshold where SerialNumber= in_SerialNumber and ProductCode=in_ProductCode and ParamName=in_Parameter;
			if(paramsexist<1) then
				Begin
					INSERT INTO `labpulse`.`deviceparamsthreshold`
					(`SerialNumber`,`ProductCode`,`DeviceName`,`IsCustomAlarmEnable`,`ParamName`,`AlertHighLevelThreshold`
					,`AlertLowLevelThreshold`,`AlarmHighLevelThreshold`,`AlarmLowLevelThreshold`,`CreatedOn`,`LastUpdatedOn`)
					VALUES(in_SerialNumber,in_ProductCode,in_DeviceName,in_IsCustomAlarmEnable,in_Parameter,in_AlertHighLevelThreshold,
					in_AlertLowLevelThreshold,in_AlarmHighLevelThreshold,in_AlarmLowLevelThreshold,NOW(),NOW());
				End;
			else
				Begin
					UPDATE `labpulse`.`deviceparamsthreshold`
					SET IsCustomAlarmEnable = in_IsCustomAlarmEnable,`ParamName` = in_Parameter,`AlertHighLevelThreshold` = in_AlertHighLevelThreshold,
					`AlertLowLevelThreshold` = in_AlertLowLevelThreshold,`AlarmHighLevelThreshold` = in_AlarmHighLevelThreshold,
					`AlarmLowLevelThreshold` = in_AlarmLowLevelThreshold,`LastUpdatedOn` = NOW()
					WHERE `SerialNumber` = in_SerialNumber and `ProductCode` = in_ProductCode and `ParamName` = in_Parameter;

				End;
			END IF;
	End;
 else
	begin
   set code='-1';
   set msg='Invalid Account';
	End;
 End IF;
 IF code = '0' THEN
    GET DIAGNOSTICS rows = ROW_COUNT;
    SET record_count = rows; 
	select LAST_INSERT_ID() into row_id from labpulse.account;
  ELSE
    set statuscode = code;
    set message = msg;
  END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_syncparametersthreshold_old
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_syncparametersthreshold_old`(
  in_SerialNumber varchar(45),
  in_Parameter varchar(45),
  in_ProductCode varchar(45),
  in_IsCustomAlarmEnable varchar(45),
  in_DeviceName varchar(45) ,  
  in_AlertHighLevelThreshold varchar(45) ,
  in_AlertLowLevelThreshold varchar(45) ,
  in_AlarmHighLevelThreshold varchar(45) ,
  in_AlarmLowLevelThreshold varchar(45) ,
  in_AccountID varchar(45),
  out record_count varchar(11),
  out row_id integer(10),
  out statuscode integer(10),
  out message varchar(100))
BEGIN
DECLARE code CHAR(5) DEFAULT '0';
DECLARE msg TEXT;
DECLARE rows INT;
DECLARE paramsexist int;
/*DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'Error occured';*/
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
    END;
SELECT count(*) into paramsexist FROM    deviceparamsthreshold where SerialNumber= in_SerialNumber and ProductCode=in_ProductCode and ParamName=in_Parameter;
if(paramsexist<1) then
	Begin
		INSERT INTO `labpulse`.`deviceparamsthreshold`
		(`SerialNumber`,`ProductCode`,`DeviceName`,`IsCustomAlarmEnable`,`ParamName`,`AlertHighLevelThreshold`
		,`AlertLowLevelThreshold`,`AlarmHighLevelThreshold`,`AlarmLowLevelThreshold`,`CreatedOn`,`LastUpdatedOn`)
		VALUES(in_SerialNumber,in_ProductCode,in_DeviceName,in_IsCustomAlarmEnable,in_Parameter,in_AlertHighLevelThreshold,
		in_AlertLowLevelThreshold,in_AlarmHighLevelThreshold,in_AlarmLowLevelThreshold,NOW(),NOW());
	End;
else
	Begin
		UPDATE `labpulse`.`deviceparamsthreshold`
		SET IsCustomAlarmEnable = in_IsCustomAlarmEnable,`ParamName` = in_Parameter,`AlertHighLevelThreshold` = in_AlertHighLevelThreshold,
		`AlertLowLevelThreshold` = in_AlertLowLevelThreshold,`AlarmHighLevelThreshold` = in_AlarmHighLevelThreshold,
		`AlarmLowLevelThreshold` = in_AlarmLowLevelThreshold,`LastUpdatedOn` = NOW()
		WHERE `SerialNumber` = in_SerialNumber and `ProductCode` = in_ProductCode and `ParamName` = in_Parameter;

	End;
END IF;
 IF code = '0' THEN
    GET DIAGNOSTICS rows = ROW_COUNT;
    SET record_count = rows; 
	select LAST_INSERT_ID() into row_id from labpulse.account;
  ELSE
    set statuscode = code;
    set message = msg;
  END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_update_cases
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_update_cases`(in_ID int ,in_CaseType varchar(45),
  in_SerialNumber varchar(45),
  in_GateWayId varchar(45),
  in_CaseReason varchar(45),
  in_Description varchar(1000),
  in_Subject varchar(200),
  in_AccountID varchar(45),
  in_CaseSFID varchar(45),
  in_LocationName varchar(45),
  in_CaseStatus  varchar(45),
  in_CaseCreatedTime datetime,
  in_CaseOrigin varchar(45),
  in_ComponentSN varchar(45),
  in_where_CaseSFID varchar(45),
in_CaseNumber varchar(45),
in_SFResponse varchar(500),
  out record_count varchar(11),
  out row_id integer(10),
  out statuscode integer(10),
  out message varchar(100))
BEGIN
DECLARE code CHAR(5) DEFAULT '0';
DECLARE msg TEXT;
DECLARE rows INT;
DECLARE result TEXT;

/*DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'Error occured';*/
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
    END;

 
	UPDATE labpulse.cases SET
	/*CaseType = in_CaseType,*/ 
    /*SerialNumber = in_SerialNumber,*/
    /*GateWayId = in_GateWayId,*/
    CaseReason = in_CaseReason,
	Description = in_Description,
    Subject=in_Subject,
    /*AccountID = in_AccountID,*/
    CaseSFID = in_CaseSFID,
    /*LocationName = in_LocationName,*/
    CaseStatus = in_CaseStatus,
    CaseCreatedTime = in_CaseCreatedTime,
    /*CaseOrigin = in_CaseOrigin*/
    /*ComponentSN = in_ComponentSN*/
CaseNumber=COALESCE(in_CaseNumber,CaseNumber),
SFResponse=COALESCE(in_SFResponse,SFResponse)
	WHERE
	CaseSFID = in_where_CaseSFID or ID=in_ID;
 
 IF code = '0' THEN
    GET DIAGNOSTICS rows = ROW_COUNT;
    SET record_count = rows; 
	select ID into row_id from labpulse.cases where 	CaseSFID = in_where_CaseSFID or ID=in_ID;
  ELSE
    set statuscode = code;
    set message = msg;
  END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_update_referenceblobconfig
-- -----------------------------------------------------

DELIMITER $$
USE `labpulse`$$
CREATE  PROCEDURE `sp_update_referenceblobconfig`(in_lastMySqlFetchedTime varchar(45),
  in_ReferenceBlobPath varchar(100),
  out record_count varchar(11),
  out row_id integer(10),
  out statuscode integer(10),
  out message varchar(100))
BEGIN
DECLARE code CHAR(5) DEFAULT '0';
DECLARE msg TEXT;
DECLARE rows INT;
DECLARE result TEXT;

/*DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'Error occured';*/
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
    END;

if ((select count(*) from labpulse.referenceblobconfig)<1)
then
INSERT INTO `labpulse`.`referenceblobconfig`
(`ID`,`lastMySqlFetchedTime`,`blobFolderPath`)
VALUES(1,in_lastMySqlFetchedTime,COALESCE(in_ReferenceBlobPath,blobFolderPath));
else
	UPDATE `labpulse`.`referenceblobconfig`
SET lastMySqlFetchedTime =in_lastMySqlFetchedTime,
blobFolderPath = COALESCE(in_ReferenceBlobPath,blobFolderPath) ; 
end if;

 IF code = '0' THEN
    GET DIAGNOSTICS rows = ROW_COUNT;
    SET record_count = rows; 
	select ID into row_id from labpulse.referenceblobconfig;
  ELSE
    set statuscode = code;
    set message = msg;
  END IF;
END$$

DELIMITER ;

DELIMITER $$
-- -----------------------------------------------------
-- procedure sp_sync_account
-- -----------------------------------------------------

CREATE DEFINER=`mirionadmin`@`%` PROCEDURE `sp_sync_account`(
    dataObject Json,  
	out record_count varchar(11),
	out row_id integer(10),
	out statuscode integer(10),
	out message varchar(500))
BEGIN
DECLARE code CHAR(5) DEFAULT '0000';
	DECLARE msg TEXT;
	DECLARE rows INT;
	DECLARE result TEXT;
    DECLARE rec_count INT;

    DECLARE  accounts, account TEXT;
    
    
    DECLARE i INT DEFAULT 0;   

    DEClare v_AccountID varchar(45);
    DEClare v_AccountName varchar(45);
    DEClare v_AccountNumber varchar(45);
    DEClare v_AccountStatus varchar(45);
    DEClare v_ParentAccount varchar(45);
    DEClare v_Email varchar(150);
    DEClare v_Phone varchar(45);
    DEClare v_Account_Type varchar(45);
    DEClare v_PreferredTechnician varchar(45);
    DEClare v_ImageURL varchar(500);	
    DECLARE current_list varchar(4000);
    
    

 

	/*DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'Error occured';*/
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
		GET DIAGNOSTICS CONDITION 1
			code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
		END;
 
		  set current_list = '';
		SELECT JSON_EXTRACT(dataObject, "$.account") INTO accounts;
        WHILE i < JSON_LENGTH(accounts) DO
       		SELECT JSON_EXTRACT(accounts,CONCAT('$[',i,']')) INTO account;



             set current_list = concat(current_list,",",JSON_EXTRACT(account, "$.AccountID"))  ;              
              
           	 SET v_AccountID = (SELECT JSON_UNQUOTE(JSON_EXTRACT(account, "$.AccountID")));	
			 
			 SET v_AccountName = (SELECT JSON_UNQUOTE(JSON_EXTRACT(account, "$.AccoutName")));
             
             SET v_AccountStatus = (SELECT JSON_UNQUOTE(JSON_EXTRACT(account, "$.AccountStatus")));
             SET v_ParentAccount = (SELECT JSON_UNQUOTE(JSON_EXTRACT(account, "$.ParentAccount")));
             SET v_Email = (SELECT JSON_UNQUOTE(JSON_EXTRACT(account, "$.Email")));
             SET v_Phone = (SELECT JSON_UNQUOTE(JSON_EXTRACT(account, "$.Phone")));
             SET v_Account_Type = (SELECT JSON_UNQUOTE(JSON_EXTRACT(account, "$.Account_Type")));
             SET v_PreferredTechnician = (SELECT JSON_UNQUOTE(JSON_EXTRACT(account, "$.PreferredTechnician")));
             SET v_ImageURL = (SELECT JSON_UNQUOTE(JSON_EXTRACT(account, "$.ImageURL")));
              
             select count(*) into rec_count from labpulse.account where AccountID = v_AccountID;
             if rec_count = 0 then
                 
				              
                insert into labpulse.account
				(AccountID,AccountName,AccountStatus,ParentAccount,Email,Phone,Account_Type,preferredTechnician,ImageURL,modifiedDate)
				values
				(v_AccountID,v_AccountName,v_AccountStatus,v_ParentAccount,v_Email,v_Phone,v_Account_Type,v_PreferredTechnician,v_ImageURL ,CURRENT_TIMESTAMP);
			
			Elseif rec_count = 1 then
			
			    UPDATE labpulse.account set
			    AccountID = v_AccountID,
			    AccountName = v_AccountName,

			    AccountStatus = v_AccountStatus,
			    ParentAccount = v_ParentAccount,
			    Email = v_Email,
			    Phone = v_Phone,
			    Account_Type = v_Account_Type,
			    PreferredTechnician = v_PreferredTechnician,
			    ImageURL = v_ImageURL, 
			    modifiedDate = CURRENT_TIMESTAMP
	            where 
	            AccountID = v_AccountID;

	            
			end if;
             
             
			SELECT i + 1 INTO i;
		END WHILE;
        
        SET current_list =  TRIM(LEADING ',' FROM current_list) ;
		-- select current_list;
        
               
    IF code = '0000' THEN
		GET DIAGNOSTICS rows = ROW_COUNT;
		SET record_count = rows; 
		select max(ID) into row_id from labpulse.account where AccountID = v_AccountID;
    
     ELSE
		 set statuscode = code;
		set message = msg;
     END IF;

END$$
DELIMITER ;


DELIMITER $$
-- -----------------------------------------------------
-- procedure sp_sync_installedprod
-- -----------------------------------------------------

CREATE DEFINER=`mirionadmin`@`%` PROCEDURE `sp_sync_installedprod`(
	dataObject Json,  
	out record_count varchar(11),
	out row_id integer(10),
	out statuscode varchar(20),
	out message varchar(500))
BEGIN

    DECLARE code CHAR(5) DEFAULT '0000';
	DECLARE msg TEXT;
	DECLARE rows INT;
	DECLARE result TEXT;
    DECLARE rec_count INT;

    DECLARE  products, product TEXT;
    
    
    DECLARE i INT DEFAULT 0;   


    DECLARE v_SerialNumber varchar(45);
	DECLARE v_InstalledProductID varchar(45);
	DECLARE v_ProductCode varchar(45);
	DECLARE v_ProductName varchar(255);
    DECLARE v_ProductDesc varchar(255);
	DECLARE v_baseProductId varchar(45);
	DECLARE v_baseProductName varchar(255);
	DECLARE v_accountID varchar(45);
    DECLARE v_locationID varchar(45);
	DECLARE v_PreferredTechnicain varchar(45);
    DECLARE v_contactName varchar(45);
    DECLARE v_email varchar(150);
    DECLARE v_mobileNo varchar(15);
	DECLARE v_TopLevelSerialNumber varchar(45);
	DECLARE v_ParentSerialNumber varchar(45);
	DECLARE v_ImageURL varchar(500);
	DECLARE v_isLabPulseReady varchar(45);
	DECLARE v_isTopLevel varchar(45);
	DECLARE v_IsOffline varchar(45);
	DECLARE v_isLabPulseConnected varchar(45);
	DECLARE v_GatewayType varchar(45);
	DECLARE v_SensorSystemID varchar(45);
	DECLARE v_SensorSystemName varchar(45);
    DECLARE current_list varchar(4000);
	DECLARE v_contactID varchar(45);
	DECLARE v_PreferredTechnicianName varchar(200);
	DECLARE v_PreferredTechnicianEmail varchar(500);
	DECLARE v_PreferredTechnicianMobile varchar(45);
    
    

 

	/*DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'Error occured';*/
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
		GET DIAGNOSTICS CONDITION 1
			code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
		END;
 
		  set current_list = '';
		SELECT JSON_EXTRACT(dataObject, "$.product") INTO products;
        WHILE i < JSON_LENGTH(products) DO
       		SELECT JSON_EXTRACT(products,CONCAT('$[',i,']')) INTO product;
			-- SELECT product;

			  -- select concat(current_list,'zzzzz') as 'first_call';
              set current_list = concat(current_list,",",JSON_UNQUOTE(JSON_EXTRACT(product, "$.InstalledProductID"))) ;
              -- select concat(current_list,JSON_EXTRACT(product, "$.InstalledProductID")) as test;
              
              
             SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.InstalledProductID")) INTO v_InstalledProductID;
			 SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.LocationId")) INTO v_locationID ;
             -- SELECT v_InstalledProductID;
                      
				SET v_SerialNumber = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.SerialNumber")));
				SET v_ProductCode = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.ProductCode")));
				SET v_ProductName = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.ProductName")));
				SET v_baseProductId = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.BaseProductId")));
				SET v_baseProductName = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.BaseProductName")));
				SET v_accountID = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.AccountId")));
				SET v_locationID = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.LocationId")));
				SET v_PreferredTechnicain = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.PreferredTechnicain")));
                SET v_contactName = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.ContactName")));
				SET v_email = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.Email")));
				SET v_mobileNo = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.MobileNo")));
				SET v_TopLevelSerialNumber = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.TopLevelSerialNumber")));
				SET v_ParentSerialNumber = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.ParentSerialNumber")));
                SET v_ImageURL = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.ImageURL")));         
				SET v_isLAbPulseReady = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.isLabPulseReady")));
                SET v_isTopLevel = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.isTopLevel")));                
				SET v_IsOffline = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.isOffline")));
				SET v_contactID = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.contactID")));
				SET v_PreferredTechnicianMobile = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.preferrefTechnicianMobile")));
                SET v_PreferredTechnicianEmail = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.preferrefTechnicianEmail")));                
				SET v_PreferredTechnicianName = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.preferrefTechnicianName")));
				SET v_isLabPulseConnected = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.labpulseConnected")));
				SET v_GatewayType = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.gatewayType")));
                SET v_SensorSystemID = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.sensorSystemID")));                
				SET v_SensorSystemName = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.sensorSystemName")));
				SET v_ProductDesc = (SELECT JSON_UNQUOTE(JSON_EXTRACT(product, "$.productDesc")));
				
				
                
             
             select count(*) into rec_count from labpulse.installed_product where InstalledProductID =  v_InstalledProductID ;
             if rec_count = 0 then



                insert into labpulse.installed_product
				(SerialNumber,installedProductId,productCode,productName,baseProductId,baseProductName,accountID,locationID,preferredTechnicain,contactName,contactEmail,contactMobileNo,topLevelSerialNumber,parentSerialNumber,ImageURL,isLabPulseReady,isTopLevel,isOffline,modifiedDate,preferredTechnicianName,preferredTechnicianEmail,preferredTechnicianMobile,contactID,labPulseConnected , GatewayType ,sensorSystemID ,sensorSystemName,prodDescription)
				values
				(upper(v_SerialNumber),v_InstalledProductID,upper(v_ProductCode),v_ProductName,v_baseProductId,v_baseProductName,v_AccountID,v_locationID, v_PreferredTechnicain, v_contactName,v_email,  v_mobileNo,  v_TopLevelSerialNumber,v_ParentSerialNumber,v_ImageURL,isLabPulseReady,v_isTopLevel,v_IsOffline,CURRENT_TIMESTAMP,v_PreferredTechnicianName,v_PreferredTechnicianEmail,v_PreferredTechnicianMobile,v_contactID,v_isLabPulseConnected,v_GatewayType,v_SensorSystemID,v_SensorSystemName,v_ProductDesc);


			Elseif rec_count = 1 then
			
			   UPDATE labpulse.installed_product set
	            SerialNumber = upper(v_SerialNumber),
	            InstalledProductID = v_InstalledProductID,
	            ProductCode = upper(v_ProductCode) ,
	            ProductName = v_ProductName,
	            baseProductId = v_baseProductId,
	            baseProductName = v_baseProductName,
	            AccountID = v_AccountID,
	            locationID = v_locationID,
	            PreferredTechnicain = v_PreferredTechnicain,
                contactName = v_contactName,
	            contactEmail = v_email,
	            contactMobileNo = v_mobileNo,	
	            TopLevelSerialNumber = v_TopLevelSerialNumber,
	            ParentSerialNumber = v_ParentSerialNumber,
                ImageURL = v_ImageURL,
	            isLabPulseReady = v_isLabPulseReady,
                isTopLevel = v_isTopLevel,
	            IsOffline = v_IsOffline,
	            modifiedDate = CURRENT_TIMESTAMP,
				contactID=v_contactID,
				preferredTechnicianName=v_PreferredTechnicianName,
				preferredTechnicianEmail=v_PreferredTechnicianEmail,
				preferredTechnicianMobile=v_PreferredTechnicianMobile,
				labPulseConnected=v_isLabPulseConnected,
				GatewayType=v_GatewayType,
				sensorSystemID=v_SensorSystemID,
				sensorSystemName=v_SensorSystemName,
				prodDescription=v_ProductDesc
where 
	            InstalledProductID = v_InstalledProductID
			    and locationID = v_locationID;
	            
			end if;
             
             
			SELECT i + 1 INTO i;
		END WHILE;
        
        SET current_list =  TRIM(LEADING ',' FROM current_list) ;
		-- select current_list;
        
		 delete from labpulse.installed_product where !FIND_IN_SET(InstalledProductID, InstalledProductID) and locationID = v_locationID ;
        			
    IF code = '0000' THEN
		GET DIAGNOSTICS rows = ROW_COUNT;
		SET record_count = rows; 
		select max(ID) into row_id from labpulse.installed_product where locationID = v_locationID;
    
     ELSE
		 set statuscode = code;
		set message = msg;
     END IF;

END$$
DELIMITER ;

DELIMITER $$
-- -----------------------------------------------------
-- procedure sp_sync_Location
-- -----------------------------------------------------
CREATE DEFINER=`mirionadmin`@`%` PROCEDURE `sp_sync_Location`(
	dataObject Json,  
	out record_count varchar(11),
	out row_id integer(10),
	out statuscode integer(10),
	out message varchar(5000))
BEGIN

 DECLARE code CHAR(5) DEFAULT '0000';
	DECLARE msg TEXT;
	DECLARE rows INT;
	DECLARE result TEXT;
    DECLARE rec_count INT;

    DECLARE  locations, location TEXT;
    
    
    DECLARE i INT DEFAULT 0;   
  

  
    DECLARE v_LocationID varchar(45);
    DECLARE V_LocationName varchar(45);
    DECLARE v_LocationType varchar(45);
    DECLARE v_Latitude varchar(45);
    DECLARE v_Longitude varchar(45);
    DECLARE v_parentAccount varchar(45);
    DECLARE v_city varchar(45);
    DECLARE v_state varchar(45);
    DECLARE v_country varchar(45);
    DECLARE v_street varchar(45);
    DECLARE v_zipcode varchar(45);
    DECLARE v_imageURL varchar(500);

    DECLARE current_list varchar(4000);
    
    

 

	/*DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'Error occured';*/
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
		GET DIAGNOSTICS CONDITION 1
			code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
		END;
 
		  set current_list = '';
		SELECT JSON_EXTRACT(dataObject, "$.location") INTO locations;
        WHILE i < JSON_LENGTH(locations) DO
       		SELECT JSON_EXTRACT(locations,CONCAT('$[',i,']')) INTO location;
			-- SELECT location;

			  -- select concat(current_list,'zzzzz') as 'first_call';
              set current_list = concat(current_list,",",JSON_EXTRACT(location, "$.LocationID"))  ;
              -- select concat(current_list,JSON_EXTRACT(location, "$.InstalledlocationID")) as test;
              
              
             SELECT JSON_UNQUOTE(JSON_EXTRACT(location, "$.LocationID")) INTO v_LocationID;
			 
			    SET V_LocationName  =  (SELECT JSON_UNQUOTE(JSON_EXTRACT(location, "$.Name")));
                -- SET v_LocationType  =  (SELECT JSON_UNQUOTE(JSON_EXTRACT(location, "$.LocationType")));
                SET v_Latitude  = (SELECT JSON_UNQUOTE(JSON_EXTRACT(location, "$.Latitude")));
                SET v_Longitude  = (SELECT JSON_UNQUOTE(JSON_EXTRACT(location, "$.Longitude")));
                SET v_parentAccount = (SELECT JSON_UNQUOTE(JSON_EXTRACT(location, "$.parentAccount")));
                SET v_city = (SELECT JSON_UNQUOTE(JSON_EXTRACT(location, "$.city")));
                SET v_state = (SELECT JSON_UNQUOTE(JSON_EXTRACT(location, "$.state")));
                SET v_country = (SELECT JSON_UNQUOTE(JSON_EXTRACT(location, "$.country")));
                SET v_street = (SELECT JSON_UNQUOTE(JSON_EXTRACT(location, "$.street")));
                SET v_zipcode = (SELECT JSON_UNQUOTE(JSON_EXTRACT(location, "$.zipcode")));
                SET v_imageURL = (SELECT JSON_UNQUOTE(JSON_EXTRACT(location, "$.imageURL")));
            
              
             select count(*) into rec_count from labpulse.location where LocationID =  v_LocationID;
             if rec_count = 0 then
			                 

                insert into labpulse.location
				(LocationID,LocationName,Latitude,Longitude,AccountID,city,state,country,street,zipcode,imageURL,modifiedDate)
				values
				(v_LocationID,V_LocationName,v_Latitude,v_Longitude,v_parentAccount,v_city, v_state, v_country, v_street,v_zipcode,v_imageURL,CURRENT_TIMESTAMP);
			
			Elseif rec_count = 1 then
			
			    UPDATE labpulse.location set
	            LocationName = V_LocationName,
	           -- LocationType = v_LocationType,
	            Latitude = v_Latitude,
	            Longitude = v_Longitude,
	            AccountID = v_parentAccount,
	            city = v_city,
	            state = v_state,
	            country = v_country,
	            street = v_street,
	            zipcode = v_zipcode,
	            imageURL = v_imageURL,
	            modifiedDate = CURRENT_TIMESTAMP
	            where 
	            LocationID = v_LocationID;

	            
			end if;
             
             
			SELECT i + 1 INTO i;
		END WHILE;
        
        
    IF code = '0000' THEN
		GET DIAGNOSTICS rows = ROW_COUNT;
		SET record_count = rows; 
		select max(ID) into row_id from labpulse.location where LocationID =  v_LocationID;
    
     ELSE
		 set statuscode = code;
		set message = msg;
     END IF;

END$$
DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
