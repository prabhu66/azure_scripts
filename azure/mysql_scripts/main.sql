-- DATABASE
CREATE DATABASE IF NOT EXISTS labpulse;
USE labpulse;

-- TABLES
source tables.sql

-- STORED PROCEDURE
source storedprocedures.sql


