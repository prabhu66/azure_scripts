-- -----------------------------------------------------
-- Table `labpulse`.`account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `labpulse`.`account` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `AccountID` VARCHAR(45) NOT NULL,
  `AccountName` VARCHAR(100) NULL DEFAULT NULL,
  `AccountStatus` VARCHAR(45) NULL DEFAULT NULL,
  `ParentAccount` VARCHAR(45) NULL DEFAULT NULL,
  `Email` VARCHAR(150) NULL DEFAULT NULL,
  `Phone` VARCHAR(45) NULL DEFAULT NULL,
  `Account_Type` VARCHAR(45) NULL DEFAULT NULL,
  `PreferredTechnician` VARCHAR(45) NULL DEFAULT NULL,
  `ImageURL` VARCHAR(500) NULL DEFAULT NULL,
  `modifiedDate` DATETIME NOT NULL,
  PRIMARY KEY (`AccountID`),
  INDEX `ID` (`ID` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `labpulse`.`cases`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `labpulse`.`cases` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `CaseType` VARCHAR(45) NULL DEFAULT NULL,
  `SerialNumber` VARCHAR(45) NULL DEFAULT NULL,
  `GateWayId` VARCHAR(45) NULL DEFAULT NULL,
  `CaseReason` VARCHAR(45) NULL DEFAULT NULL,
  `Description` VARCHAR(1000) NULL DEFAULT NULL,
  `AccountID` VARCHAR(45) NULL DEFAULT NULL,
  `CaseSFID` VARCHAR(45) NULL DEFAULT NULL,
  `LocationName` VARCHAR(45) NULL DEFAULT NULL,
  `CaseStatus` VARCHAR(45) NULL DEFAULT NULL,
  `CaseCreatedTime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CaseOrigin` VARCHAR(45) NULL DEFAULT NULL,
  `ComponentSN` VARCHAR(45) NULL DEFAULT NULL,
  `AlarmType` VARCHAR(45) NULL DEFAULT NULL,
  `Subject` VARCHAR(200) NULL DEFAULT NULL,
  `ProductCode` VARCHAR(45) NULL DEFAULT NULL,
  `DeviceID` VARCHAR(45) NULL DEFAULT NULL,
  `CaseNumber` VARCHAR(45) NULL DEFAULT NULL,
  `SFResponse` VARCHAR(500) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  INDEX `fk_acc` (`AccountID` ASC),
  CONSTRAINT `cases_ibfk_1`
    FOREIGN KEY (`AccountID`)
    REFERENCES `labpulse`.`account` (`AccountID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 188
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `labpulse`.`deviceparamsthreshold`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `labpulse`.`deviceparamsthreshold` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `SerialNumber` VARCHAR(45) NULL DEFAULT NULL,
  `ProductCode` VARCHAR(45) NULL DEFAULT NULL,
  `DeviceName` VARCHAR(45) NULL DEFAULT NULL,
  `IsCustomAlarmEnable` VARCHAR(45) NULL DEFAULT NULL,
  `ParamName` VARCHAR(100) NULL DEFAULT NULL,
  `AlertHighLevelThreshold` VARCHAR(45) NULL DEFAULT NULL,
  `AlertLowLevelThreshold` VARCHAR(45) NULL DEFAULT NULL,
  `AlarmHighLevelThreshold` VARCHAR(45) NULL DEFAULT NULL,
  `AlarmLowLevelThreshold` VARCHAR(45) NULL DEFAULT NULL,
  `CreatedOn` DATETIME NULL DEFAULT NULL,
  `LastUpdatedOn` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `uniqueparam` (`SerialNumber` ASC, `ProductCode` ASC, `ParamName` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 289
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `labpulse`.`installed_product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `labpulse`.`installed_product` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `serialNumber` VARCHAR(45) NULL DEFAULT NULL,
  `installedProductID` VARCHAR(45) NOT NULL,
  `productCode` VARCHAR(45) NULL DEFAULT NULL,
  `productName` VARCHAR(45) NULL DEFAULT NULL,
  `baseProductId` VARCHAR(45) NULL DEFAULT NULL,
  `baseProductName` VARCHAR(45) NULL DEFAULT NULL,
  `accountID` VARCHAR(45) NULL DEFAULT NULL,
  `locationID` VARCHAR(45) NOT NULL,
  `preferredTechnicain` VARCHAR(45) NULL DEFAULT NULL,
  `contactName` VARCHAR(45) NULL DEFAULT NULL,
  `contactEmail` VARCHAR(145) NULL DEFAULT NULL,
  `contactMobileNo` VARCHAR(15) NULL DEFAULT NULL,
  `topLevelSerialNumber` VARCHAR(45) NULL DEFAULT NULL,
  `parentSerialNumber` VARCHAR(45) NULL DEFAULT NULL,
  `ImageURL` VARCHAR(500) NULL DEFAULT NULL,
  `isLabPulseReady` VARCHAR(45) NULL DEFAULT NULL,
  `isTopLevel` VARCHAR(45) NULL DEFAULT NULL,
  `isOffline` VARCHAR(45) NULL DEFAULT NULL,
  `modifiedDate` DATETIME NOT NULL,
  `contactID` VARCHAR(45) NULL DEFAULT NULL,
  `preferredTechnicianName` VARCHAR(200) NULL DEFAULT NULL,
  `preferredTechnicianEmail` VARCHAR(200) NULL DEFAULT NULL,
  `preferredTechnicianMobile` VARCHAR(45) NULL DEFAULT NULL,
  `GatewayType` VARCHAR(45) NULL DEFAULT NULL,
  `sensorSystemID` VARCHAR(45) NULL DEFAULT NULL,
  `sensorSystemName` VARCHAR(45) NULL DEFAULT NULL,
  `labPulseConnected` VARCHAR(45) NULL DEFAULT NULL,
  `prodDescription` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`installedProductID`),
  UNIQUE INDEX `installedProductID` (`installedProductID` ASC),
  INDEX `id` (`id` ASC),
  INDEX `fk_acc` (`accountID` ASC),
  CONSTRAINT `installed_product_ibfk_1`
    FOREIGN KEY (`accountID`)
    REFERENCES `labpulse`.`account` (`AccountID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 785
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `labpulse`.`location`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `labpulse`.`location` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `LocationID` VARCHAR(45) NOT NULL,
  `LocationName` VARCHAR(45) NULL DEFAULT NULL,
  `AccountID` VARCHAR(45) NULL DEFAULT NULL,
  `City` VARCHAR(45) NULL DEFAULT NULL,
  `State` VARCHAR(45) NULL DEFAULT NULL,
  `Country` VARCHAR(45) NULL DEFAULT NULL,
  `Street` VARCHAR(45) NULL DEFAULT NULL,
  `Zipcode` VARCHAR(45) NULL DEFAULT NULL,
  `LocationContact` VARCHAR(45) NULL DEFAULT NULL,
  `ImageURL` VARCHAR(500) NULL DEFAULT NULL,
  `Latitude` VARCHAR(45) NULL DEFAULT NULL,
  `Longitude` VARCHAR(45) NULL DEFAULT NULL,
  `modifiedDate` DATETIME NOT NULL,
  PRIMARY KEY (`LocationID`),
  UNIQUE INDEX `LocationID` (`LocationID` ASC),
  INDEX `ID` (`ID` ASC),
  INDEX `fk_acc` (`AccountID` ASC),
  CONSTRAINT `location_ibfk_1`
    FOREIGN KEY (`AccountID`)
    REFERENCES `labpulse`.`account` (`AccountID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 17
DEFAULT CHARACTER SET = latin1;

USE `labpulse` ;
-- -----------------------------------------------------
-- Table `labpulse`.`notifications`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `labpulse`.`notifications` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `NotificationType` VARCHAR(45) NULL DEFAULT NULL,
  `CaseID` VARCHAR(45) NULL DEFAULT NULL,
  `Email` VARCHAR(45) NULL DEFAULT NULL,
  `MobileNumber` VARCHAR(45) NULL DEFAULT NULL,
  `EmailSubject` VARCHAR(500) NULL DEFAULT NULL,
  `EmailBody` VARCHAR(1000) NULL DEFAULT NULL,
  `SMS` VARCHAR(256) NULL DEFAULT NULL,
  `NotificationTimestamp` DATETIME NOT NULL,
  `Notificationstatus` VARCHAR(45) NULL DEFAULT NULL,
  INDEX `ID` (`ID` ASC),
  INDEX `fk_case` (`CaseID` ASC),
  CONSTRAINT `notifications_ibfk_1`
    FOREIGN KEY (`CaseID`)
    REFERENCES `labpulse`.`cases_1` (`CaseSFID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `labpulse`.`referenceblobconfig`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `labpulse`.`referenceblobconfig` (
  `ID` INT(11) NOT NULL,
  `lastMySqlFetchedTime` VARCHAR(45) NULL DEFAULT NULL,
  `blobFolderPath` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;



-- -----------------------------------------------------
-- Table `labpulse`.`applicationversion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `labpulse`.`applicationversion` (
  `ApplicationName` varchar(200) NOT NULL,
  `ApplicationLabel` varchar(200) DEFAULT NULL,
  `version` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ApplicationName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;